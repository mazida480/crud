import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  baseUrl:string = 'http://localhost:3000'
  
  httpOptions:{} = {
    headers: new HttpHeaders({'content-type':'application/json'})
  }
  
  constructor(private http:HttpClient, private messageService:MessageService) { }

  private log(message:string){
    this.messageService.addMessage(message)
  }

  private handleError<T>(operation = 'operation', result?:T){
    return (error:any):Observable<T>  => {
      console.log(error)
      this.log(`${operation} failed: ${error.message}`)
      return of(result as T)

    }
  }

  getUsers():Observable<User[]>{
    return this.http.get<User[]>(`${this.baseUrl}/users`).pipe(
      tap(_ => this.log(`Fetched users`)),
      catchError(this.handleError<User[]>('getUsers', []))
    )
  }

  getUserById(id:string):Observable<User>{
    return this.http.get<User>(`${this.baseUrl}/user/${id}`).pipe(
      tap(_ => this.log(`Fetched user with id: ${id}`)),
      catchError(this.handleError<User>(`getUser id: ${id}`))
    )
  }

  createUser(user:User):Observable<User>{
    return this.http.post<User>(`${this.baseUrl}/create-user`, user, this.httpOptions).pipe(

      tap((newUser:any) => {
        console.log(newUser)
        this.log(`Created user: id=${newUser}`)
      })
      ,

      catchError(this.handleError<User>(`createUser`))
    )
  }

  updateUser(user:User, id:string):Observable<any>{
    return this.http.put(`${this.baseUrl}/update-user/${id}`, user, this.httpOptions).pipe(
      tap(_ => this.log(`Updated user with id: ${id}`)),

      catchError(this.handleError<any>(`updateUser`))
    )
  }

  deleteUser(id:string):Observable<User>{
    return this.http.delete<User>(`${this.baseUrl}/delete-user/${id}`).pipe(
      tap(_ => this.log(`Deleted user with id: ${id}`)),

      catchError(this.handleError<User>(`deleteUser`))
    )
  }

}
