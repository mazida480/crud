import { Component } from '@angular/core';
import { CrudService } from 'src/app/shared/crud.service';
import { User } from 'src/app/shared/user';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent {

  user!:User

  constructor(private crudService:CrudService, private activateRoute:ActivatedRoute){
    const id = this.activateRoute.snapshot.params['id']

    this.getUserDetail(id)

  }

  getUserDetail(id:string){
    this.crudService.getUserById(id).subscribe(user => {
      this.user = user
    })
  }

}
