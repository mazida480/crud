import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/shared/crud.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  users:any[] = []
  filteredUserList:any[] = []

  constructor(private crudServer:CrudService){

  }

  getUsers(){
    this.crudServer.getUsers().subscribe(users => {
      this.users = users
      this.filteredUserList = users
    })
  }

  ngOnInit(): void {
    this.getUsers()

  }

  filterResults(text:string){
  }

}
