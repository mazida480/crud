import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { CrudService } from 'src/app/shared/crud.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit{

  userId!:string
  constructor(private crudService:CrudService, private activeRoute:ActivatedRoute, private location:Location, private router:Router){}

  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe((param:ParamMap) => this.userId = <string>param.get('id'))
  }

  delete(){
    this.crudService.deleteUser(this.userId).subscribe(res => {
      console.log(res)
      this.router.navigate(['/users'])
    
    })
  }

  back(){
    this.location.back()
  }

}
