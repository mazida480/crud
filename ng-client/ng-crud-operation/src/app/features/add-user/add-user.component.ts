import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService } from 'src/app/shared/crud.service';
import { User } from 'src/app/shared/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit{

  user!:User 
  userForm!:FormGroup
  constructor(private crudService:CrudService, private fb:FormBuilder){}


  onSubmit(){
    this.user = <User>this.userForm.value
    this.crudService.createUser(this.user).subscribe(res => {
      console.log(res)
    })
  }

  ngOnInit(): void {

    this.userForm = this.fb.group({
      first_name:['', Validators.required],
      last_name:['', Validators.required],
      email:['', Validators.required]
    })
  
    
  }


}
