import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { CrudService } from 'src/app/shared/crud.service';
import { User } from 'src/app/shared/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  user!:User
  id!:string

  constructor(private fb:FormBuilder, private crudService:CrudService, private activeRoute:ActivatedRoute, private location:Location){}

  userForm:FormGroup = this.fb.group({
    first_name:['', Validators.required],
    last_name:['', Validators.required],
    email:['', Validators.required]
  })

  onSubmit(){
    this.user = this.userForm.value
    
    this.crudService.updateUser(this.user, this.id).subscribe(res => {

      console.log(res)
      this.location.back()
    })
  }

  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe((param:ParamMap) => {
      this.id = <string>param.get('id')

      this.crudService.getUserById(this.id).subscribe(user => {

        
        /* Pre-populate form values. */
        this.userForm.setValue({
          first_name:user.first_name,
          last_name:user.last_name,
          email:user.email
        })
      })
    })


  }
}
