import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/shared/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit{

  messages:string[] = []

  constructor(public messageService:MessageService){}

  ngOnInit(): void {
    this.messages = this.messageService.messages
  }


  clear(){
    console.log('clear')
    this.messageService.clear()
  }



}
