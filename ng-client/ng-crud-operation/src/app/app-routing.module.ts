import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { UserlistComponent } from './features/userlist/userlist.component';
import { UserDetailComponent } from './features/user-detail/user-detail.component';
import { UpdateUserComponent } from './features/update-user/update-user.component';
import { AddUserComponent } from './features/add-user/add-user.component';
import { DeleteUserComponent } from './features/delete-user/delete-user.component';

const routes: Routes = [
  {path:'', redirectTo:'/dashboard', pathMatch:'full'},

  {path:'dashboard', component:DashboardComponent, title:'Dashboard'},

  {path:'users', component:UserlistComponent, title:'Users'},

  {path:'user/:id', component:UserDetailComponent,title:'Detail'},

  {path:'update-user/:id', component:UpdateUserComponent, title:'Update'},

  {path:'create-user', component:AddUserComponent, title:'Add User'}, 

  {path:'delete-user/:id', component:DeleteUserComponent, title:'Delete User'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
