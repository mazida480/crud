import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserlistComponent } from './features/userlist/userlist.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserDetailComponent } from './features/user-detail/user-detail.component';
import { MessageComponent } from './features/message/message.component';
import { UpdateUserComponent } from './features/update-user/update-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AddUserComponent } from './features/add-user/add-user.component';
import { DeleteUserComponent } from './features/delete-user/delete-user.component';
@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    DashboardComponent,
    UserDetailComponent,
    MessageComponent,
    UpdateUserComponent,
    AddUserComponent,
    DeleteUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
