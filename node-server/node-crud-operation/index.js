const express = require('express')
const body_parser = require('body-parser')
const cors = require('cors')
const app = express()
const port = 3000
const router = require('./router/router')

/* Database and schema */
const database = require('./db/mongodb')
const schema = require('./schema/user_schema')

app.use(body_parser.json())
app.use(body_parser.urlencoded({extended:false}))
app.use(cors({origin:'*'}))

app.listen(port, () => {
    console.log(`Server is running at port ${port}.`)
})

app.use('', router)