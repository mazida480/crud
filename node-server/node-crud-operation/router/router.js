const express = require('express')
const router = express.Router()
const User = require('../schema/user_schema')



router.get('/', (req, res) => {
    console.log(req.query)
    res.send('Home page')
})


/* Get all users */
router.get('/users', async (req, res) => {

        await User.find().then(users => {
            res.send(users)
        }).catch(err => {
            res.send(err, 'Failed to fetch users.')
        })

})

/* Create one user */
router.post('/create-user', async (req, res) => {
    const isUserExit = await User.findOne({email:req.body.email})

    console.log(isUserExit)

    if(isUserExit){
        res.send({"message":"This email is used."})
    }else{

        const newUser = new User(req.body)
    
        newUser.save()
        res.send({newUser})
    }
})

/* Get one user with its id */
router.get('/user/:id', async (req, res) => {
    await User.findOne({_id:req.params.id}).then(user => {
        res.send(user)
    }).catch(err => {
        res.send(err, 'Unable to send you the user you requested for.')
    })

})

/* Update a user with its ID.*/
router.put('/update-user/:id', async (req, res) => {
    const targetUser = await User.findById(req.params.id)

    targetUser.first_name = req.body.first_name
    targetUser.last_name = req.body.last_name
    targetUser.email = req.body.email

    targetUser.save()

    res.send(targetUser)
})

router.delete('/delete-user/:id', async (req, res) => {
    const targetUser = await User.deleteOne({_id:req.params.id})

    if(targetUser.deletedCount > 0){

        res.send(targetUser)
    }else{
        res.send({"message":"User not found"})
    }

})

module.exports = router